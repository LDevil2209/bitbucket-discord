<?php
namespace BitbucketDiscord;

require_once(__DIR__ . '/Bitbucket.php');

class Comment extends Bitbucket {
    private $text = null;
    private $link = null;
    public function __construct($array, $discord_url) {
        parent::__construct($array, $discord_url);
        if (!is_array($array)) {
            $array = json_decode($array, true);
        }
        if (isset($array['comment'])) {
            $this->name($this->userName());
            $this->avatar($this->userAvatar());
            $this->text = $array['comment']['content']['raw'];
            if (isset($array['comment']['commit'])) {
                $this->link = $array['comment']['commit']['links']['html']['href'];
            }
            $this->appendPhrase($this->text);
            $this->appendPhrase($this->link);
        }
    }

    public function commentText() {
        return $this->text;
    }
}