<?php
namespace BitbucketDiscord;

class Repository {
    public $avatar = null;
    public $link = null;
    public $name = null;
    public $type = null;
    public $id = null;
    public $is_private = null;

    public function __construct($array) {
        if (!is_array($array)) {
            $array = (array) $array;
        }
        if (isset($array['links']['avatar'])) {
            $this->avatar = $array['links']['avatar']['href'];
        }
        if (isset($array['links']['html'])) {
            $this->link = $array['links']['html']['href'];
        }
        if (isset($array['name'])) {
            $this->name = $array['name'];
        }
        if (isset($array['type'])) {
            $this->type = $array['type'];
        }
        if (isset($array['uuid'])) {
            $this->id = $array['uuid'];
        }
        if (isset($array['is_private'])) {
            $this->is_private = $array['is_private'] == true;
        }
    }
}