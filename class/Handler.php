<?php
namespace BitbucketDiscord;

require_once(__DIR__ . '/Bitbucket.php');
require_once(__DIR__ . '/Comment.php');

class Handler {
    private $request = null;
    private $array = null;
    private $config = null;
    private $discord_url = null;
    private $bitbucket = null;

    function __construct($request, $config) {
        $this->config = $config;
        if (isset($config['discord_url'])) {
            $this->discord_url = $config['discord_url'];
        }
        $this->request = $request;
        $this->array = json_decode($this->request, true);

        if (isset($this->array['comment'])) {
            $this->bitbucket = new Comment($this->array, $this->discord_url);
            return $this;
        }
        return $this;
    }

    public function send() {
        $this->bitbucket->send();
    }

    public function test($request, $config) {
    }
}