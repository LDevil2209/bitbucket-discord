<?php
namespace BitbucketDiscord;

require_once(__DIR__ . '/Discord.php');
require_once(__DIR__ . '/User.php');
require_once(__DIR__ . '/Repository.php');

/*
 * Стандартный класс, принимающий запрос от Bitbucket,
 * позволяющий отправлять сообщения на Discord.
 * @array - входящий JSON-запрос;
 * @discord_url - ссылка на Discord Webhook execute;
 */
class Bitbucket {
    private $user = null;
    private $repository = null;
    private $discord = null;
    private $message = '';

    public function __construct($array, $discord_url) {
        if (!is_array($array)) {
            $array = json_decode($array, true);
        }
        if (isset($array['actor'])) {
            $this->user = new \BitbucketDiscord\User($array['actor']);
        }
        if (isset($array['repository'])) {
            $this->repository = new \BitbucketDiscord\Repository($array['repository']);
        }
        $this->discord = new Discord($discord_url);
    }

    /*
     * Отправляет сообщение.
     */
    public function send($message = '') {
        if (empty($message)) {
            $message = $this->message;
            $this->clear();
        }
        if (!empty($message)) {
            $this->discord->send($message);
            return true;
        } else {
            return false;
        }
    }

    /*
     * Очищает аватар и имя отправителя,
     * чтобы отправка происходила от лица Webhook.
     */
    public function clearSender() {
        $this->name(null);
        $this->avatar(null);
    }

    /*
     * Заменяет стандартный аватар вебхука.
     * @avatar - URL аватара;
     */
    public function avatar($avatar) {
        $this->discord->avatar($avatar);
    }

    /*
     * Заменяет стандартное имя вебхука.
     * @name - новое имя;
     */
    public function name($name) {
        $this->discord->name($name);
    }

    /*
     * Очищает сообщение.
     */
    public function clear() {
        $this->message = '';
    }

    /*
     * Добавляет текст в конец сообщения.
     */
    public function append($text) {
        $this->message .= $text;
        return $this->message;
    }

    /*
     * Добавляет предложение в конец сообщения.
     * Предложения разделяются точкой.
     */
    public function appendPhrase($text) {
        if (!empty($this->message)) {
            $this->append('. ');
        }
        return $this->append($text);
    }

    /*
     * Методы, возвращающие свойства User
     */
    public function userName() {
        return (!is_null($this->user)) ? $this->user->name : null;
    }
    public function userLink() {
        return (!is_null($this->user)) ? $this->user->link : null;
    }
    public function userLogin() {
        return (!is_null($this->user)) ? $this->user->login : null;
    }
    public function userType() {
        return (!is_null($this->user)) ? $this->user->type : null;
    }
    public function userID() {
        return (!is_null($this->user)) ? $this->user->id : null;
    }
    public function userAvatar() {
        return (!is_null($this->user)) ? $this->user->avatar : null;
    }

    /*
     * Методы, возвращающие свойства репозитория
     */
    public function repName() {
        return (!is_null($this->repository)) ? $this->repository->name : null;
    }
    public function repLink() {
        return (!is_null($this->repository)) ? $this->repository->link : null;
    }
    public function repType() {
        return (!is_null($this->repository)) ? $this->repository->type : null;
    }
    public function repID() {
        return (!is_null($this->repository)) ? $this->repository->id : null;
    }
    public function repAvatar() {
        return (!is_null($this->repository)) ? $this->repository->avatar : null;
    }

    /*
     * Приватный ли репозиторий
     */
    public function isPrivate() {
        return (!is_null($this->repository)) ? $this->repository->is_private : false;
    }
}