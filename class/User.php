<?php
namespace BitbucketDiscord;

class User {
    public $avatar = null;
    public $link = null;
    public $login = null;
    public $name = null;
    public $type = null;
    public $id = null;

    public function __construct($array) {
        if (!is_array($array)) {
            $array = (array) $array;
        }
        if (isset($array['links']['avatar'])) {
            $this->avatar = $array['links']['avatar']['href'];
        }
        if (isset($array['links']['html'])) {
            $this->link = $array['links']['html']['href'];
        }
        if (isset($array['username'])) {
            $this->login = $array['username'];
        }
        if (isset($array['display_name'])) {
            $this->name = $array['display_name'];
        }
        if (isset($array['type'])) {
            $this->type = $array['type'];
        }
        if (isset($array['uuid'])) {
            $this->id = $array['uuid'];
        }
    }
}